# README #

This is a light weight app to store and display critical information for clients. When not needed, it tucks away to the side, then expands on demand. You can copy values out at the click of a button, and only information you want it to display and store will be visible. At its smallest, it collapses to a 50x50 circle, half of it laying off screen.