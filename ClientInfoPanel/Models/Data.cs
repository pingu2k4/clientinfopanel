﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientInfoPanel.Models
{
    class Data : INPC
    {
        public string FirstName
        {
            get
            {
                return Properties.Settings.Default.FirstName;
            }
            set
            {
                Properties.Settings.Default.FirstName = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("FirstName");
                OnPropertyChanged("FullName");
                OnPropertyChanged("Initials");
            }
        }

        public string LastName
        {
            get
            {
                return Properties.Settings.Default.LastName;
            }
            set
            {
                Properties.Settings.Default.LastName = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("LastName");
                OnPropertyChanged("FullName");
                OnPropertyChanged("Initials");
            }
        }

        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
            set { }
        }

        public string Initials
        {
            get
            {
                if (FirstName.Length == 0)
                {
                    if (LastName.Length == 0)
                        return "??";
                    return $"?{LastName.ToUpper()[0]}";
                }
                else if (LastName.Length == 0)
                    return $"{FirstName.ToUpper()[0]}?";
                return $"{FirstName.ToUpper()[0]}{LastName.ToUpper()[0]}";
            }
            set { }
        }

        public string BankSecurity
        {
            get
            {
                return Properties.Settings.Default.BankSecurity;
            }
            set
            {
                Properties.Settings.Default.BankSecurity = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("BankSecurity");
            }
        }

        public string Phone
        {
            get
            {
                return Properties.Settings.Default.Phone;
            }
            set
            {
                Properties.Settings.Default.Phone = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("Phone");
            }
        }

        public string DOB
        {
            get
            {
                return Properties.Settings.Default.DOB;
            }
            set
            {
                Properties.Settings.Default.DOB = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("DOB");
            }
        }

        public string Email
        {
            get
            {
                return Properties.Settings.Default.Email;
            }
            set
            {
                Properties.Settings.Default.Email = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("Email");
            }
        }

        public string CSV
        {
            get
            {
                return Properties.Settings.Default.CSV;
            }
            set
            {
                Properties.Settings.Default.CSV = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("CSV");
            }
        }

        public string Expiry
        {
            get
            {
                return Properties.Settings.Default.Expiry;
            }
            set
            {
                Properties.Settings.Default.Expiry = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("Expiry");
            }
        }

        public string CardNumber
        {
            get
            {
                return Properties.Settings.Default.CardNumber;
            }
            set
            {
                Properties.Settings.Default.CardNumber = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("CardNumber");
            }
        }

        public string Address
        {
            get
            {
                return Properties.Settings.Default.Address;
            }
            set
            {
                Properties.Settings.Default.Address = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("Address");
            }
        }

        public string PostCode
        {
            get
            {
                return Properties.Settings.Default.PostCode;
            }
            set
            {
                Properties.Settings.Default.PostCode = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("PostCode");
            }
        }

        public string Notes
        {
            get
            {
                return Properties.Settings.Default.Notes;
            }
            set
            {
                Properties.Settings.Default.Notes = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("Notes");
            }
        }
    }
}
