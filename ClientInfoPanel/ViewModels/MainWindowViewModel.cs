﻿using ClientInfoPanel.Commands;
using ClientInfoPanel.Enum;
using ClientInfoPanel.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ClientInfoPanel.ViewModels
{
    class MainWindowViewModel : VMBase
    {
        public MainWindowViewModel()
        {
            if (Properties.Settings.Default.NeedUpgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.NeedUpgrade = false;

                Properties.Settings.Default.Save();
            }
        }

        public MainWindowViewModel(Window mainWindow) : this()
        {
            this.mainWindow = mainWindow;
        }

        #region METHODS
        public void OnBobbleClicked()
        {
            switch (State)
            {
                case EState.CLOSED:
                    State = EState.OPEN;
                    break;
                default:
                    System.Windows.Forms.MessageBox.Show("ERROR: Expected state to be closed.");
                    break;
            }
        }

        internal void WindowClosing(CancelEventArgs e)
        {
            Properties.Settings.Default.WindowTop = WindowTop;
            Properties.Settings.Default.Save();
        }

        public override void Dispose()
        {

        }
        #endregion

        #region EVENTS

        #endregion

        #region PROPERTIES
        private double _WindowTop = Properties.Settings.Default.WindowTop;
        public double WindowTop
        {
            get
            {
                return _WindowTop;
            }
            set
            {
                _WindowTop = value;
                OnPropertyChanged("WindowTop");
            }
        }

        private double _WindowLeft = -25;
        public double WindowLeft
        {
            get
            {
                return _WindowLeft;
            }
            set
            {
                _WindowLeft = value;
                OnPropertyChanged("WindowLeft");
            }
        }

        private EState _State = EState.CLOSED;
        public EState State
        {
            get
            {
                return _State;
            }
            set
            {
                EState previousState = _State;
                _State = value;
                OnPropertyChanged("State");
                switch (value)
                {
                    case EState.CLOSED:
                        WindowWidth = 50;
                        WindowLeft = -25;
                        break;
                    case EState.OPEN:
                        WindowWidth = 350;
                        if (previousState == EState.CLOSED && WindowLeft < 10)
                            WindowLeft = 10;
                        break;
                    case EState.EDIT:
                        WindowWidth = 350;
                        break;
                    default:
                        break;
                }
            }
        }

        private double _WindowWidth = 50;
        public double WindowWidth
        {
            get
            {
                return _WindowWidth;
            }
            set
            {
                _WindowWidth = value;
                OnPropertyChanged("WindowWidth");
            }
        }

        private Data _Data = new Data();
        public Data Data
        {
            get
            {
                return _Data;
            }
            set
            {
                _Data = value;
                OnPropertyChanged("Data");
            }
        }
        #endregion

        #region VARIABLES
        Window mainWindow;
        #endregion

        #region COMMANDS
        private ICommand _CloseInfo;
        public ICommand CloseInfo
        {
            get
            {
                if (_CloseInfo == null)
                {
                    _CloseInfo = new RelayCommand(CloseInfoEx, null);
                }
                return _CloseInfo;
            }
        }
        private void CloseInfoEx(object p)
        {
            if (State == EState.EDIT)
                State = EState.OPEN;
            else
                State = EState.CLOSED;
        }

        private ICommand _EditInfo;
        public ICommand EditInfo
        {
            get
            {
                if (_EditInfo == null)
                {
                    _EditInfo = new RelayCommand(EditInfoEx, null);
                }
                return _EditInfo;
            }
        }
        private void EditInfoEx(object p)
        {
            if (State == EState.EDIT)
                State = EState.OPEN;
            else
                State = EState.EDIT;
        }
        #endregion
    }
}
