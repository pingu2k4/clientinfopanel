﻿using ClientInfoPanel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientInfoPanel.ViewModels
{
    public abstract class VMBase : INPC, IDisposable
    {
        /// <summary>
        /// Disposes the VM
        /// </summary>
        public abstract void Dispose();
    }
}
