﻿using ClientInfoPanel.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientInfoPanel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            DataContext = new MainWindowViewModel(this);
            InitializeComponent();
        }

        private void DragWindow(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Point originalLocation = new Point(this.Left, this.Top);

                this.DragMove();
                if (originalLocation.X == this.Left && originalLocation.Y == this.Top)
                {
                    (DataContext as MainWindowViewModel).OnBobbleClicked();
                }
                if (this.Left < -25)
                    this.Left = -25;
            }
            else
            {
                (DataContext as MainWindowViewModel).OnBobbleClicked();
            }
        }

        private void DragOpenWindow(object sender, MouseButtonEventArgs e)
        {
            if(e.LeftButton == MouseButtonState.Pressed)
                this.DragMove();
        }

        private void OnClose(object sender, System.ComponentModel.CancelEventArgs e)
        {
            (DataContext as MainWindowViewModel).WindowClosing(e);
        }
    }
}
