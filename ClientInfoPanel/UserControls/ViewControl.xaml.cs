﻿using ClientInfoPanel.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientInfoPanel.UserControls
{
    /// <summary>
    /// Interaction logic for ViewControl.xaml
    /// </summary>
    public partial class ViewControl : UserControl
    {
        public ViewControl()
        {
            InitializeComponent();
        }

        public string Prop
        {
            get { return (string)GetValue(PropProperty); }
            set { SetValue(PropProperty, value); }
        }
        public static readonly DependencyProperty PropProperty =
            DependencyProperty.Register("Prop", typeof(string), typeof(ViewControl), new PropertyMetadata(null));

        public string PropName
        {
            get { return (string)GetValue(PropNameProperty); }
            set { SetValue(PropNameProperty, value); }
        }
        public static readonly DependencyProperty PropNameProperty =
            DependencyProperty.Register("PropName", typeof(string), typeof(ViewControl), new PropertyMetadata(null));

        private ICommand _Copy;
        public ICommand Copy
        {
            get
            {
                if (_Copy == null)
                {
                    _Copy = new RelayCommand(CopyEx, null);
                }
                return _Copy;
            }
        }
        private void CopyEx(object p)
        {
            Clipboard.SetText(Prop);
        }
    }
}
