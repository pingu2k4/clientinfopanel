﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientInfoPanel.UserControls
{
    /// <summary>
    /// Interaction logic for EditControl.xaml
    /// </summary>
    public partial class EditControl : UserControl
    {
        public EditControl()
        {
            InitializeComponent();
        }

        public string Prop
        {
            get { return (string)GetValue(PropProperty); }
            set { SetValue(PropProperty, value); }
        }
        public static readonly DependencyProperty PropProperty =
            DependencyProperty.Register("Prop", typeof(string), typeof(EditControl), new PropertyMetadata(string.Empty));

        public string PropName
        {
            get { return (string)GetValue(PropNameProperty); }
            set { SetValue(PropNameProperty, value); }
        }
        public static readonly DependencyProperty PropNameProperty =
            DependencyProperty.Register("PropName", typeof(string), typeof(EditControl), new PropertyMetadata("Label"));
    }
}
